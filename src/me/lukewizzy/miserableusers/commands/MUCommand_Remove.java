package me.lukewizzy.miserableusers.commands;

import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class MUCommand_Remove extends SubCommand {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String[] args) {
        OfflinePlayer oP = Bukkit.getOfflinePlayer(args[1]);
        
        if (!MiserableUsers.getMiserableUsers().getMiserableUsers().contains(oP.getUniqueId().toString())) {
            sender.sendMessage(MiserableUsers.getMessagePrefix() + ChatColor.RED + args[1] + " is not being made miserable.");
            return true;
        }
        
        MiserableUsers.getMiserableUsers().removeMiserableUser(oP.getUniqueId().toString());
        sender.sendMessage(MiserableUsers.getMessagePrefix() + args[1] + " will no longer be miserable.");
        return false;
    }
}
