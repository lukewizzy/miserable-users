package me.lukewizzy.miserableusers.commands;

import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class MUCommand_Add extends SubCommand {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String[] args) {
        OfflinePlayer oP = Bukkit.getOfflinePlayer(args[1]);
        
        if (MiserableUsers.getMiserableUsers().getMiserableUsers().contains(oP.getUniqueId().toString())) {
            sender.sendMessage(MiserableUsers.getMessagePrefix() + ChatColor.RED + args[1] + " is already being made miserable.");
            return true;
        }
        
        MiserableUsers.getMiserableUsers().addMiserableUser(oP.getUniqueId().toString());
        sender.sendMessage(MiserableUsers.getMessagePrefix() + args[1] + " will now be made miserable.");
        return false;
    }
}
