package me.lukewizzy.miserableusers.commands;

import me.lukewizzy.miserableusers.util.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class MUCommand_Help extends SubCommand {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String[] args) {
        sender.sendMessage(ChatColor.GRAY + "----------------------");
        sender.sendMessage(ChatColor.YELLOW + "/" + cmd.getName() + " add <player>" + ChatColor.DARK_GRAY + " - " + ChatColor.DARK_AQUA + "Add a miserable user.");
        sender.sendMessage(ChatColor.YELLOW + "/" + cmd.getName() + " remove <player>" + ChatColor.DARK_GRAY + " - " + ChatColor.DARK_AQUA + "Remove a miserable user.");
        sender.sendMessage(ChatColor.YELLOW + "/" + cmd.getName() + " clear" + ChatColor.DARK_GRAY + " - " + ChatColor.DARK_AQUA + "Clear the miserable users.");
        sender.sendMessage(ChatColor.GRAY + "----------------------");
        return false;
    }
}
