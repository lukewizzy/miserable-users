package me.lukewizzy.miserableusers.commands;

import me.lukewizzy.miserableusers.MiserableUsers;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MiserableUsersCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.isOp()) {
            sender.sendMessage(MiserableUsers.getMessagePrefix() + ChatColor.RED + "You do not have permission to use this command.");
        }
        
        if (args.length == 2 && args[0].equalsIgnoreCase("add")) {
            new MUCommand_Add().onCommand(sender, cmd, args);
        }
        else if (args.length > 1 && args[0].equalsIgnoreCase("clear") && sender.hasPermission("miserableusers.command.clear")) {
            new MUCommand_Clear().onCommand(sender, cmd, args);
        }
        else if (args.length == 2 && args[0].equalsIgnoreCase("remove")) {
            new MUCommand_Remove().onCommand(sender, cmd, args);
        }
        else {
            new MUCommand_Help().onCommand(sender, cmd, args);
        }
        return false;
    }
}
