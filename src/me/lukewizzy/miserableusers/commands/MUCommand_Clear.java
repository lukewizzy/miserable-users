package me.lukewizzy.miserableusers.commands;

import java.util.ArrayList;
import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class MUCommand_Clear extends SubCommand {
    public static boolean waitingForConfirmation;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String[] args) {
        if (waitingForConfirmation && args.length == 2 && args[1].equalsIgnoreCase("confirm")) {
            MiserableUsers.getMiserableUsers().setMiserableUsers(new ArrayList<String>());
            sender.sendMessage(MiserableUsers.getMessagePrefix() + ChatColor.GREEN + "The list of miserable users has been reset.");
        }
        else {
            sender.sendMessage(MiserableUsers.getMessagePrefix() + "This will reset the list of miserable users. If you're sure you want to continue, type /" + cmd.getName() + " clear confirm");
            waitingForConfirmation = true;
            Bukkit.getScheduler().scheduleSyncDelayedTask(MiserableUsers.getInstance(), new Runnable() {
                public void run() {
                    waitingForConfirmation = false;
                }
            }, 20 * 20L);
            return false;
        }
        return false;
    }
}
