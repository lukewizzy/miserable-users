package me.lukewizzy.miserableusers.timers;

import java.util.ArrayList;
import java.util.Collections;
import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.MiserableAction;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class RandomDisconnections implements Runnable {

    public static ArrayList<String> dcMessages = new ArrayList<>();

    @Override
    public void run() {
        for (Player ps : Bukkit.getOnlinePlayers()) {
            if (MiserableUsers.getActionChance().willHappen(ps, MiserableAction.DISCONNECTION)) {
                Collections.shuffle(dcMessages);
                ps.kickPlayer(dcMessages.get(0));
                MiserableUsers.getInstance().getLogger().info(ps.getName() + " was disconnected for " + dcMessages.get(0));
            }
        }
    }
}
