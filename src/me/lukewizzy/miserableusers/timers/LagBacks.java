package me.lukewizzy.miserableusers.timers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.MiserableAction;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LagBacks implements Runnable {

    public HashMap<String, Location> playerLocations = new HashMap<>();
    int count;

    @Override
    public void run() {
        if (count > 4) {
            count++;
        }
        else {
            count = 1;
        }

        for (Player ps : Bukkit.getOnlinePlayers()) {
            if (count == 4) {
                if (MiserableUsers.getActionChance().willHappen(ps, MiserableAction.LAG_BACK)) {
                    if (playerLocations.containsKey(ps.getName())) {
                        ps.teleport(playerLocations.get(ps.getName()));
                        MiserableUsers.getInstance().getLogger().info(ps.getName() + " got lagged back.");
                    }
                }
            }
            else {
                if (MiserableUsers.getMiserableUsers().getMiserableUsers().contains(ps.getName())) {
                    if (playerLocations.containsKey(ps.getName())) {
                        playerLocations.remove(ps.getName());
                    }
                    playerLocations.put(ps.getName(), ps.getLocation());
                }
            }
        }
    }
}
