package me.lukewizzy.miserableusers.timers;

import java.util.ArrayList;
import java.util.Collections;
import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.MiserableAction;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class MobSpawning implements Runnable {
    
    public static ArrayList<EntityType> overWorldMobs = new ArrayList<>();
    public static ArrayList<EntityType> netherMobs = new ArrayList<>();
    
    @Override
    public void run() {
        for (Player ps : Bukkit.getOnlinePlayers()) {
            if (MiserableUsers.getActionChance().willHappen(ps, MiserableAction.MOB_SPAWN)) {
                EntityType ent;
                Collections.shuffle(overWorldMobs);
                Collections.shuffle(netherMobs);
                if (ps.getWorld().equals(Bukkit.getWorlds().get(1))) {
                    ent = netherMobs.get(0);
                }
                else if (ps.getWorld().equals(Bukkit.getWorlds().get(2))) {
                    ent = EntityType.ENDERMAN;
                }
                else {
                    ent = overWorldMobs.get(0);
                }
                
                ps.getWorld().spawnEntity(ps.getLocation(), ent);
                MiserableUsers.getInstance().getLogger().info(ps.getName() + " had a " + ent.name() + " spawned on them.");
            }
        }
    }
    
}
