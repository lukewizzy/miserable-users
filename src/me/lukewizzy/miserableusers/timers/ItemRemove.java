package me.lukewizzy.miserableusers.timers;

import java.util.ArrayList;
import java.util.Collections;
import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.MiserableAction;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ItemRemove implements Runnable {
    
    private ArrayList<Integer> slots = new ArrayList<>();
    
    @Override
    public void run() {
        if (slots.size() <= 0) {
            for (int i = 0; i < 27; i++) {
                slots.add(i);
            }
        }
        for (Player ps : Bukkit.getOnlinePlayers()) {
            if (MiserableUsers.getActionChance().willHappen(ps, MiserableAction.ITEM_REMOVE)) {
                Collections.shuffle(slots);
                Material mat;
                int slot = slots.get(0);
                
                try {
                    mat = ps.getInventory().getItem(slot).getType();
                    ps.getInventory().setItem(slot, new ItemStack(mat, ps.getInventory().getItem(slot).getAmount()-1));
                    
                    
                    MiserableUsers.getInstance().getLogger().info(ps.getName() + " had 1 " + mat.name() + " removed from their inventory.");
                } catch (NullPointerException ex) {
                    
                }
            }
        }
    }
    
}
