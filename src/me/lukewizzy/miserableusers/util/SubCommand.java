package me.lukewizzy.miserableusers.util;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public abstract class SubCommand {

    public abstract boolean onCommand(CommandSender sender, Command cmd, String[] args);
}
