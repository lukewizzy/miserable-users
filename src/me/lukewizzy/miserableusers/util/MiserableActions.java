package me.lukewizzy.miserableusers.util;

import java.util.ArrayList;
import java.util.Collections;
import me.lukewizzy.miserableusers.MiserableUsers;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class MiserableActions {

    ArrayList<Integer> chance = new ArrayList<>();

    public boolean willHappen(Player p, MiserableAction action) {
        if (!MiserableUsers.getMiserableUsers().getMiserableUsers().contains(p.getUniqueId().toString())) {
            return false;
        }
        if (p.hasPermission("miserableusers.bypass")) {
            return false;
        }
        if (!action.isEnabled()) {
            return false;
        }
        ArrayList<Integer> frequency = new ArrayList<>();

        for (int i = 1; i <= action.getFrequency(); i++) {
            frequency.add(i);
        }
        Collections.shuffle(frequency);

        if (frequency.get(0) == 0) {
            return true;
        }
        Collections.shuffle(chance);

        if (chance.get(0) <= action.getChance()) {
            return true;
        }
        return false;
    }

    public void setupChances() {
        chance.clear();
        for (int i = 1; i <= 10; i++) {
            chance.add(i);
        }
    }
}
