package me.lukewizzy.miserableusers.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import me.lukewizzy.miserableusers.MiserableUsers;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class MiserableUsersList {

    private List<String> miserableUsers = new ArrayList<>();
    private File miserableUsersFile = null;
    private FileConfiguration miserableUsersConfig = null;

    public List<String> getMiserableUsers() {
        return miserableUsers;
    }
    
    public void setMiserableUsers(List<String> users) {
        miserableUsers = users;
    }

    public void addMiserableUser(String uuid) {
        if (!miserableUsers.contains(uuid)) {
            miserableUsers.add(uuid);
        }
    }

    public void removeMiserableUser(String uuid) {
        if (miserableUsers.contains(uuid)) {
            miserableUsers.remove(uuid);
        }
    }

    @SuppressWarnings("deprecation")
    public void reloadMiserableUsers() {
        if (miserableUsersFile == null) {
            miserableUsersFile = new File(MiserableUsers.getInstance().getDataFolder(), "miserableusers.yml");
        }
        miserableUsersConfig = YamlConfiguration.loadConfiguration(miserableUsersFile);

        // Look for defaults in the jar
        InputStream defConfigStream = MiserableUsers.getInstance().getResource("miserableusers.yml");
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            miserableUsersConfig.setDefaults(defConfig);
        }
    }

    public FileConfiguration getMiserableUsersConfig() {
        if (miserableUsersConfig == null) {
            reloadMiserableUsers();
        }
        return miserableUsersConfig;
    }

    public void saveMiserableUsers() {
        if (miserableUsersFile == null || miserableUsersConfig == null) {
            return;
        }
        try {
            getMiserableUsersConfig().save(miserableUsersFile);
        }
        catch (IOException ex) {
            MiserableUsers.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + miserableUsersConfig, ex);
        }
    }

    public void saveDefaultUsers() {
        if (miserableUsersFile == null) {
            miserableUsersFile = new File(MiserableUsers.getInstance().getDataFolder(), "miserableusers.yml");
        }
        if (!miserableUsersFile.exists()) {
            MiserableUsers.getInstance().saveResource("miserableusers.yml", false);
        }
    }
}
