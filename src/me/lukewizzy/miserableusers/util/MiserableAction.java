package me.lukewizzy.miserableusers.util;

public enum MiserableAction {

    CHAT_FAIL(3), DISCONNECTION(1), INTERACT_REMOVE(5), ITEM_REMOVE(1), LAG_BACK(4), MOB_SPAWN(1), NO_PLACE(5), SILVERFISH(10);

    private int chance;
    private boolean enabled;
    private int frequency;

    private MiserableAction(int frequency) {
        this.frequency = frequency;
    }

    public int getChance() {
        return chance;
    }

    public void setChance(int chance) {
        this.chance = chance;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getFrequency() {
        return frequency;
    }
}
