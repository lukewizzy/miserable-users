package me.lukewizzy.miserableusers;

import java.io.File;
import java.util.Arrays;
import me.lukewizzy.miserableusers.commands.MUCommand_Clear;
import me.lukewizzy.miserableusers.commands.MiserableUsersCommand;
import me.lukewizzy.miserableusers.events.BlockBreak;
import me.lukewizzy.miserableusers.events.BlockPlace;
import me.lukewizzy.miserableusers.events.PlayerChat;
import me.lukewizzy.miserableusers.events.PlayerInteract;
import me.lukewizzy.miserableusers.timers.ItemRemove;
import me.lukewizzy.miserableusers.timers.LagBacks;
import me.lukewizzy.miserableusers.timers.MobSpawning;
import me.lukewizzy.miserableusers.timers.RandomDisconnections;
import me.lukewizzy.miserableusers.util.MiserableAction;
import me.lukewizzy.miserableusers.util.MiserableActions;
import me.lukewizzy.miserableusers.util.MiserableUsersList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class MiserableUsers extends JavaPlugin {
    
    private static String messagePrefix = ChatColor.DARK_AQUA + "[" + ChatColor.GOLD + "MiserableUsers" + ChatColor.DARK_AQUA + "] " + ChatColor.YELLOW;
    private static MiserableUsers instance;
    private static final MiserableUsersList miserableUsersClass = new MiserableUsersList();
    private static final MiserableActions chance = new MiserableActions();

    @Override
    public void onEnable() {
        instance = this;

        // Register commands
        getCommand("mu").setExecutor(new MiserableUsersCommand());
        getCommand("miserableusers").setExecutor(new MiserableUsersCommand());
        MUCommand_Clear.waitingForConfirmation = false;
        
        // Register events
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new BlockBreak(), this);
        pm.registerEvents(new BlockPlace(), this);
        pm.registerEvents(new PlayerChat(), this);
        pm.registerEvents(new PlayerInteract(), this);

        // Create config
        if (!new File(getDataFolder(), "config.yml").exists()) {
            saveDefaultConfig();
            getConfig().options().copyDefaults(true);
        }

        // Create miserable users list
        if (!new File(getDataFolder(), "miserableusers.yml").exists()) {
            getMiserableUsers().saveDefaultUsers();
            getMiserableUsers().getMiserableUsersConfig().options().copyDefaults(true);
        }
        
        getMiserableUsers().setMiserableUsers(getMiserableUsers().getMiserableUsersConfig().getStringList("miserableusers"));
        
        // Add disconnection errors
        RandomDisconnections.dcMessages.add("Internal exception: java.io.UTFDataFormatException: malformed input around byte 5");
        RandomDisconnections.dcMessages.add("Internal client error: java.net.Connect.Exception: connection has timed out");
        RandomDisconnections.dcMessages.add("Failed to connect to the server");
        RandomDisconnections.dcMessages.add("Internal exception: java.io.IOException: Server returned HTTP response code: 503");
        RandomDisconnections.dcMessages.add("Internal exception: java.net.SocketException: Connection reset");
        RandomDisconnections.dcMessages.add("Internal exception: Took to long to log in");
        RandomDisconnections.dcMessages.add("Internal exception: java.lang.NullPointerException");
        RandomDisconnections.dcMessages.add("Internal exception: RuntimeException: No OpenGL context found in the current thread.");
        RandomDisconnections.dcMessages.add("Internal exception: RuntimeException: org.lwjgl.LWJGLException: Pixel format not accelerated");
        
        // Add mob types
        MobSpawning.overWorldMobs.add(EntityType.CAVE_SPIDER);
        MobSpawning.overWorldMobs.add(EntityType.CREEPER);
        MobSpawning.overWorldMobs.add(EntityType.ENDERMAN);
        MobSpawning.overWorldMobs.add(EntityType.LIGHTNING);
        MobSpawning.overWorldMobs.add(EntityType.PRIMED_TNT);
        MobSpawning.overWorldMobs.add(EntityType.SILVERFISH);
        MobSpawning.overWorldMobs.add(EntityType.SKELETON);
        MobSpawning.overWorldMobs.add(EntityType.SLIME);
        MobSpawning.overWorldMobs.add(EntityType.SPIDER);
        MobSpawning.overWorldMobs.add(EntityType.WITCH);
        MobSpawning.overWorldMobs.add(EntityType.ZOMBIE);
        
        MobSpawning.netherMobs.add(EntityType.BLAZE);
        MobSpawning.netherMobs.add(EntityType.FIREBALL);
        MobSpawning.netherMobs.add(EntityType.GHAST);
        MobSpawning.netherMobs.add(EntityType.PIG_ZOMBIE);
        MobSpawning.netherMobs.add(EntityType.SMALL_FIREBALL);

        // Update config data
        MiserableAction.CHAT_FAIL.setEnabled(getConfig().getBoolean("chatfailenabled"));
        MiserableAction.CHAT_FAIL.setChance(getConfig().getInt("chatfailfrequency"));
        
        MiserableAction.DISCONNECTION.setEnabled(getConfig().getBoolean("disconnectionenabled"));
        MiserableAction.DISCONNECTION.setChance(getConfig().getInt("disconnectionfrequency"));
        
        MiserableAction.INTERACT_REMOVE.setEnabled(getConfig().getBoolean("interactremoveenabled"));
        MiserableAction.INTERACT_REMOVE.setChance(getConfig().getInt("interactremovefrequency"));
        
        MiserableAction.ITEM_REMOVE.setEnabled(getConfig().getBoolean("itemremoveenabled"));
        MiserableAction.ITEM_REMOVE.setChance(getConfig().getInt("itemremovefrequency"));
        
        MiserableAction.LAG_BACK.setEnabled(getConfig().getBoolean("lagbackenabled"));
        MiserableAction.LAG_BACK.setChance(getConfig().getInt("lagbackfrequency"));
        
        MiserableAction.MOB_SPAWN.setEnabled(getConfig().getBoolean("mobspawningenabled"));
        MiserableAction.MOB_SPAWN.setChance(getConfig().getInt("mobspawningfrequency"));
        
        MiserableAction.NO_PLACE.setEnabled(getConfig().getBoolean("noplaceenabled"));
        MiserableAction.NO_PLACE.setChance(getConfig().getInt("noplacefrequency"));
        
        MiserableAction.SILVERFISH.setEnabled(getConfig().getBoolean("silverfishenabled"));
        MiserableAction.SILVERFISH.setChance(getConfig().getInt("silverfishfrequency"));

        // Start timers
        if (MiserableAction.DISCONNECTION.isEnabled()) {
            getServer().getScheduler().scheduleSyncRepeatingTask(this, new ItemRemove(), 20L, 30 * 20L);
            getServer().getScheduler().scheduleSyncRepeatingTask(this, new LagBacks(), 20L, 5 * 20L);
            getServer().getScheduler().scheduleSyncRepeatingTask(this, new MobSpawning(), 20L, 30 * 20L);
            getServer().getScheduler().scheduleSyncRepeatingTask(this, new RandomDisconnections(), 20L, 60 * 20L);
        }

        getActionChance().setupChances();
    }

    @Override
    public void onDisable() {
        getMiserableUsers().getMiserableUsersConfig().set("miserableusers", getMiserableUsers().getMiserableUsers());
        getMiserableUsers().saveMiserableUsers();
        instance = null;
        getServer().getScheduler().cancelAllTasks();
    }

    public static MiserableUsers getInstance() {
        return instance;
    }
    
    public static MiserableActions getActionChance() {
        return chance;
    }
    
    public static MiserableUsersList getMiserableUsers() {
        return miserableUsersClass;
    }
    
    public static String getMessagePrefix() {
        return messagePrefix;
    }
}
