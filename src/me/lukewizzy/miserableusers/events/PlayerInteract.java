package me.lukewizzy.miserableusers.events;

import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.MiserableAction;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteract implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Action a = e.getAction();
        if (a == Action.RIGHT_CLICK_BLOCK || a == Action.PHYSICAL) {
            Player p = e.getPlayer();
            if (MiserableUsers.getActionChance().willHappen(p, MiserableAction.DISCONNECTION)) {
                e.getClickedBlock().setType(Material.AIR);
                MiserableUsers.getInstance().getLogger().info(p.getName() + " had a block they interacted with destroyed.");
            }
        }
    }
}
