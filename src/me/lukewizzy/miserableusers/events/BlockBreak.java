package me.lukewizzy.miserableusers.events;

import java.util.Random;
import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.MiserableAction;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreak implements Listener {

    @EventHandler
    public void onBlockBreak(final BlockBreakEvent e) {
        if (MiserableUsers.getActionChance().willHappen(e.getPlayer(), MiserableAction.SILVERFISH)) {
            MiserableUsers.getInstance().getLogger().info("Silverfish were spawned on " + e.getPlayer().getName());
            for (int i = 0; i < (new Random().nextInt(3)) + 2; i++) {
                MiserableUsers.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(MiserableUsers.getInstance(),
                        new Runnable() {
                            public void run() {
                                e.getBlock().getWorld().spawnEntity(e.getBlock().getLocation(), EntityType.SILVERFISH);
                            }
                        }, i * 10L);
            }
        }
    }
}
