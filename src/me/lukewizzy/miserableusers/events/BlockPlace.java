package me.lukewizzy.miserableusers.events;

import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.MiserableAction;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

public class BlockPlace implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (MiserableUsers.getActionChance().willHappen(e.getPlayer(), MiserableAction.NO_PLACE)) {
            e.setCancelled(true);
            ItemStack hand = e.getPlayer().getItemInHand();
            hand.setAmount(hand.getAmount() - 1);
            MiserableUsers.getInstance().getLogger().info(e.getPlayer().getName() + "'s block was not placed.");
        }
    }
}
