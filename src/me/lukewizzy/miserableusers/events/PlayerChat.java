package me.lukewizzy.miserableusers.events;

import me.lukewizzy.miserableusers.MiserableUsers;
import me.lukewizzy.miserableusers.util.MiserableAction;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChat implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        if (MiserableUsers.getActionChance().willHappen(e.getPlayer(), MiserableAction.CHAT_FAIL)) {
            MiserableUsers.getInstance().getLogger().info(e.getPlayer().getName() + "'s chat message was cancelled.");
            e.setCancelled(true);
        }
    }
}
